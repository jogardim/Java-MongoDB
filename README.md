# MongoDB

# Para conectar no mongo
mongo

<hr>

# Listar todas as bases de dados
show dbs

<hr>

# Criar ou usar uma base
use meu_banco

<hr>

# Verificar em qual base de dados você esta no MongoDB
db

<hr>

# Listando todas as coleções de um banco
show collections

<hr>

# Criando uma coleção(tabela)
db.createCollection("contato")

<hr>

# Inserindo registro
<pre>
db.contato.insert({"nome":"Gardim", "data_nascimento":new Date(1988, 01, 26)})
db.contato.insert({
    "nome": "Felipe",
    "data_nascimento": new Date(1994, 02, 26),
    "curso": {
        "nome": "Sistemas de informação"
    },
    "notas": [10.0, 9.0, 4.5],
    "habilidades": [{
        "nome": "inglês",
        "nível": "avançado"
    }, {
        "nome": "taekwondo",
        "nível": "básico"
    }]
})
</pre>

# Excluindo registro
<pre>
db.contato.remove({
    "_id" : ObjectId ("56cb0002b6d75ec12f75d3b5")
})
</pre>

<hr>

# Listando os registros
db.contato.find()

<hr>

# Pegando contato com nome 'Pedro'
db.contato.find( {"nome":"pedro"} )

<hr>

# Listando contatos com habilidades 'inglês'
db.contato.find({"habilidade.nome":"inglês"})

<hr>

# Listando contatos com habilidades com nível 'Avançado' e com nome Pedro
db.contato.find({"habilidade.nível":"Avançado", "nome":"Pedro"})

<hr>

 # Listando contatos com habilidades com nível 'Avançado' OU 'Intermediário'
 <pre>
db.contato.find({
$or: [
       {"habilidade.nível":"Avançado"},
       {"habilidade.nível":"Intermediário"}
     ]
})
</pre>

<hr>

# Listando contatos com habilidades com nível 'Avançado' OU 'Intermediário' E nome 'Pedro'
<pre>
db.contato.find(
   {
      $or: [
              {"habilidade.nível":"Avançado"},
              {"habilidade.nível":"Intermediário"}
           ],
      "nome":"Pedro"
   }
)
</pre>

<hr>

 # Listando contatos com habilidades com nível 'Avançado' OU 'Intermediário' E nome 'Pedro' usando operador $in
 <pre>
db.contato.find(
   {
      "habilidade.nível" : { $in : ["Avançado", "Intermediário"] },
      "nome":"Pedro"
   }
)
</pre>

<br>

<pre>
# UPDATE SEM SET SUBSTITUI O OBJETO DO PRIMEIRO PARÂMETRO PELO OBJETO DO SEGUNDO PARÂMETRO # Cuidado
db.contato.update(
   {"habilidade.nível":"Avançado"},
   {"nível":"Avançado"}
)
</pre>

<hr>

# Atualiza um campo do primeiro registro (no update, o primeiro parâmetro é a query)
<pre>
db.contato.update(
   {
      "habilidade.nível":"Avançado"
   },
   {
      $set : {"habilidade.nível" : "Fluente"}
   }
)
</pre>

<hr>

# Atualiza um campo de todos os registros
<pre>
db.contato.update(
   {
      "habilidade.nível":"Avançado"
   },
   {
      $set : {"habilidade.nível" : "Fluente"}
   },
   {
      "multi" : "true"
   }
)
</pre>

<hr>

# Atualiza um registro inteiro
<pre>
db.contato.update({"_id": ObjectId("1234565435")},
{
    "nome": "Daniela",
    "data_nascimento": new Date(1997, 07, 17),
    "notas": [10, 9, 4],
    "curso": {
        "nome": "Moda"
    },
    "habilidades": [
        {
            "nome": "Alemão",
            "nível": "Básico"
        }
    ]
})
</pre>

<hr>

# Atualiza o registro mesmo não tendo o campo curso. Neste caso, ele cria o campo sozinho
<pre>
db.contato.update(
   {"_id": ObjectId("590393636e3b01ba5c30e4bb")},
   {
      $set : {
         "curso.nome" : "Análise de Sistemas"
       }
    }
)
</pre>

<hr>

# Busca todos os contatos que o valor da nota seja > 5 (gt = Greater than = maior que)
<pre>
db.contato.find(
  {
    "notas" : { $gt : 5}
  }
)
</pre>

<hr>

# Busca todos os contatos que o valor da nota seja < 5 (lt = menor que)
<pre>
db.contato.find({
    notas : { $lt : 5}
})
</pre>

<hr>

# Busca somente um resultado
<pre>
db.contato.findOne(
  {
    "notas" : { $gt : 5}
  }
)

</pre>

<hr>

# Busca ordenado por nome em ordem crescente
db.contato.find().sort({"nome" : 1})

<hr>

# Busca ordenado por nome em ordem decrescente
db.contato.find().sort({"nome" : -1})

<hr>

# Busca ordenado por nome em ordem crescente limitado à três registros
db.contato.find().sort({"nome" : 1}).limit(3)

<hr>

# Busca contato mais próximo de [-23.123456, -46.123456]
<pre>
db.contato.aggregate(
  [
    {
      $geoNear : {
        "near" : {
          "coordinates" : [-23.123456, -46.123456],
          "type" : "Point"
        },
        "distanceField" : "distancia.calculada",
        "spherical" : "true",
        "num" : 4
      }
    },
    {
      $skip : 1
    }
  ]
)
</pre>

<hr>

# Cria índice de localização para o campo 'localizacao' para ser usado no aggregate
<pre>
db.contato.createIndex(
  {
    "localizacao" : "2dsphere"
  }
)
</pre>
.
